# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
MyApp::Application.config.secret_key_base = 'a0e1163258109a4a59f0cd63fe01ab4674f1bb0b8b8e86df8697c93e3e63e5a5289785c7e80abe3d1e906ac0dd6c1907c77b5d67bed29cb498d2c7a1b6d1a929'
